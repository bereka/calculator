package com.example.calculator

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.trimmedLength
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    private lateinit var resultTextView: TextView
    private lateinit var resultText: TextView

    private var operand: Double = 0.0

    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)
        resultText = findViewById(R.id.output)
    }

    fun numberClick(clickedView: View) {

        if (clickedView is TextView) {

            var result: String = resultTextView.text.toString()
            var number: String = clickedView.text.toString()


            if (result == "0") {
                result = ""
            }

            resultTextView.text = result + number
        }

    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {

            var result: String = resultTextView.text.toString()

            if (result.isNotEmpty()) {
                this.operand = result.toDouble()

            }
            resultTextView.text = ""

            this.operation = clickedView.text.toString()

        }
    }

    fun equalsClick(clickedView: View) {

        val result: String = resultTextView.text.toString()

        var secOperand: Double = 0.0

        if (result.isNotEmpty()) {

            secOperand = result.toDouble()


        }
        var res = ""
        when (operation) {
            "+" -> res = (operand + secOperand).toString()
            "-" -> res = (operand - secOperand).toString()
            "*" -> res = (operand * secOperand).toString()
            "/" -> res = (operand / secOperand).toString()
            "√" -> res = sqrt(operand).toString()
            "x²" -> res = (operand * operand).toString()
            "%" -> res = (operand * (secOperand/100)).toString()

        }
        if (res.endsWith(".0")) {
            res = res.dropLast(2)
        }
        resultText.text = res
    }

    fun clearClick(clickedView: View) {

        resultText.text = " "
        resultTextView.text = " "
    }

    fun delClick(clickedView: View) {

        val length = resultTextView.text.trimmedLength()
        if (length > 0) {
            resultTextView.text = resultTextView.text.subSequence(0, length - 1)

        }
    }
}

